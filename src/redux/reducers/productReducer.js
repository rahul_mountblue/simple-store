import { actionTypes } from "../constant/action-type";

const initialState = {
    products: [],
}
export const productReducer = (state = initialState, { type, payload }) => {

    switch (type) {
        case actionTypes.SET_PRODUCTS:
            return { ...state, products: payload };

        case actionTypes.ADD_PRODUCT:
            return {
                ...state,
                products: [...state.products, payload]
            }

        case actionTypes.DELETE_PRODUCT:
            console.log(state);
            return {
                products: [
                    ...state.products.filter((product) => product.id !== payload)
                ]
            }

        default:
            return state;

    }
};
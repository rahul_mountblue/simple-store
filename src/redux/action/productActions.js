import {actionTypes} from '../constant/action-type';

export const setProducts = (products) => {
    
         return {
             type:actionTypes.SET_PRODUCTS,
             payload : products,
         }
}

export const addProducts = (product)=>{
     return {
         type:actionTypes.ADD_PRODUCT,
         payload : product
     }
}

export const deleteProductById = (id)=>{
    console.log(id);
   
    return {
        type:actionTypes.DELETE_PRODUCT,
        payload:id
    }
}

export const selectedProduct = (product) => {
    return {
        type:actionTypes.SELECTED_PRODUCT,
        payload:product,
    }
}
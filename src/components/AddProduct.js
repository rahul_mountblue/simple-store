import React, { Component } from 'react'

class AddProduct extends Component {
    constructor(props) {
        super(props)

    }
    handleClick = () => {
        this.props.addproduct(this.props.products);
    }

    render() {
         console.log(this.props);

        return (
            <div className='container mt-3 mb-3 text-center'>
                <button className='btn btn-primary' onClick={this.handleClick}>Add Product</button>
            </div>
        )
    }
}

export default AddProduct;
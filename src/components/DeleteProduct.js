import React, { Component } from 'react';

export default class DeleteProduct extends Component {
    constructor(props) {
        super(props);
    }
    handleClick=()=>{
        this.props.deleteproduct(this.props.id);
    }
    render() {
        return <div>
             <i className="fa fa-trash" style={{fontSize:"20px",color:"red"}} onClick={this.handleClick}></i>

            {/* <button type="button" class="btn btn-danger" onClick={this.handleClick}>Delete</button> */}
        </div>;
    }
}

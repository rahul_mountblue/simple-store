import React, { Component } from 'react';
import LoginIllu from '../images/loginillustration.jpg'
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import LoginLogout from './LoginLogout';
import {Redirect} from 'react-router-dom'

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            remember: false,
            login: true,
            logout: false,
            res:'',
        }

    }

    handleChange = (event) => {
        const { name, value } = event.target;
        if (event.target.type === 'checkbox') {
            this.setState({ [name]: event.target.checked })
        } else {
            this.setState({ [name]: value })
        }
    }

    loginSuccessWithGoogle = (res) => {
        console.log(res);
        this.setState({res:res.profileObj,
            login: false,
            logout: true
        })
    }


    loginFailureWithGoogle(res) {
        console.log(res);
    }

    logoutSuccess = () => {
        console.log('logout');
        this.setState({
            login: true,
            logout: false,
        })
    }

    render() {
        const clientId = '765530767805-7bbpvagak3dea9d7dbt9pfcbdhctp8tv.apps.googleusercontent.com';
        const { login, logout } = this.state;
        console.log(logout);
        // if(logout === true) {
        //     return <Redirect to='/' />
        // }
        return <div className='container'>
            <div className="row justify-content-center align-items-center border mb-3">
                <div className="col-sm-6 col-md-4">
                    <img src={LoginIllu} alt="login illustration" className='img-fluid' />
                </div>
                <div className="col-sm-4 col-md-3">
                    <form className='mt-4 '>
                        <div className="mb-3 text-center">
                            {login ?
                                <GoogleLogin
                                    clientId={clientId}
                                    buttonText="Login with google"
                                    onSuccess={this.loginSuccessWithGoogle}
                                    onFailure={this.loginFailureWithGoogle}
                                    
                                    cookiePolicy={'single_host_origin'}
                                />
                                :
                                null
                            }

                            {
                                logout ?
                                    <GoogleLogout
                                        clientId={clientId}
                                        render={renderProps => (
                                            <LoginLogout res={this.state.res} logoutSuccess={this.logoutSuccess} logout={this.state.logout}/>
                                          )}
                                        onLogoutSuccess={this.logoutSuccess}
                                    >
                                    </GoogleLogout>
                                    :
                                    null
                            }

                            <p className='text-secondary'>or</p>

                        </div>
                        <div className="form-group mb-2">

                            <input type="email"
                                className="form-control"
                                id="exampleInputEmail1"
                                onChange={this.handleChange}
                                defaultValue={this.state.email}
                                placeholder="Enter email"
                            />

                        </div>
                        <div className="form-group mb-2">

                            <input type="password"
                                className="form-control"
                                name="remember"
                                id="InputPassword1"
                                onChange={this.handleChange}
                                defaultValue={this.state.password}
                                placeholder="Password"
                            />

                        </div>
                        <div className="form-group form-check">

                            <input type="checkbox"
                                onChange={this.handleChange}
                                className="form-check-input"
                                id="Check1"
                            />
                            <label className="form-check-label" htmlFor="Check1">Remember me</label>
                        </div>
                        <button type="submit" className="btn btn-primary w-100 mb-2">Signin</button>
                    </form>
                </div>
            </div>


        </div>;
    }
}

export default Login;

import React, { Component } from 'react'
import {Link} from 'react-router-dom';

class Categories extends Component {
    render() {
        return (
            <div className="container">
                       <nav className='d-flex justify-content-center'>
                           <Link to="/men" className='p-2'>Mens</Link>
                           <Link to="/women" className='p-2'>Womens</Link>
                           <Link to="/electronic" className='p-2'> Electronics </Link>
                       </nav>
                </div>
        )
    }
}

export default  Categories;

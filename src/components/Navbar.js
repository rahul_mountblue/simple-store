import React, { Component } from 'react';
import SS from '../images/sslogo.jpg';
import LoginLogout from './LoginLogout';
import { Link } from 'react-router-dom';

class Navbar extends Component {
    constructor(props) {
        super(props)
}

    render() {
        console.log('inside navbar');
        return (

            <nav className="navbar navbar-expand-lg navbar-light justify-content-between bg-light border-bottom border-2 border-warning p-0">
                <div className="navbar-brand" href="#">
                    <img src={SS} alt="store logo" className="rounded-circle" style={{ height: '40px', width: '40px' }} />
                    <span className='position-relative' style={{ top: "-15px" }}> Simple <span className='text-warning'>Store</span></span>
                </div>
                <div className="navbar mr-auto">
                    <LoginLogout />  
                    <Link to="" className='nav-link'>
                        <i className="fa fa-shopping-cart" style={{ fontSize: "24px" }}></i>
                    </Link>

                </div>
            </nav>

        )
    }
}
export default Navbar;

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class LoginLogout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showLogout: false,
            name: '',
            image: '',
        }
    }
    componentDidMount() {
        if (this.props.logout) {

            this.setState({ showLogout: true })
        } else {
            this.setState({ showLogout: false })   
        }
    }

    handleClick = () => {
         
        this.props.logoutSuccess();
    }

    render() {
        // console.log(this.state.showLogout)

        if (this.state.showLogout) {
            return <div>
                <img src="" alt="" /><span>rahul majhi</span>
                <p onClick={this.handleClick}>Logout</p>
            </div>
        } else {
            return <div>


            <Link to="/login" className='nav-link text-primary'>Login</Link>

        </div>
        }

        
    }
}

export default LoginLogout;

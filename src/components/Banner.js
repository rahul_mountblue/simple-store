import React, { Component } from 'react'
import BanImage from '../images/banner2.jpg'

class Banner extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className='row'>

                    <div className='col-md-12 p-0'>
                        <img src={BanImage} alt="banner" className='w-100 img-fluid ' style={{height:"300px"}} />
                    </div>
                </div>

            </div>
        )
    }
}

export default Banner;

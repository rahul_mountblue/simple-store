import React, { Component } from 'react'
import axios from 'axios';
import ReactStars from "react-rating-stars-component";
import AddProduct from './AddProduct';
import UpdateProducts from './UpdateProducts';
import DeleteProduct from './DeleteProduct';
import { connect } from 'react-redux';
import { setProducts, addProducts, deleteProductById } from '../redux/action/productActions';


const mapStateToProps = (props) => {
    return {
        allProducts: props.allProducts.products,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setProducts: (products) => dispatch(setProducts(products)),
        addProducts: (product) => dispatch(addProducts(product)),
        deleteProductById: (id) => dispatch(deleteProductById(id)),
    }
}



class Products extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataFetched: false,
            networkError: '',
        }
    }

    componentDidMount() {
        this.getProductData();
    }

    async getProductData() {
        try {
            let products = await axios.get('https://fakestoreapi.com/products');
            this.props.setProducts(products.data);
            this.setState({
                dataFetched: true,
            })

        } catch (error) {
            this.setState({
                networkError: 'Something went wrong, Please try again!'
            })
            console.log(error);
        }
    }

    addProduct = async (products) => {
        let data = products;
        try {

            let res = await axios.post('https://fakestoreapi.com/products',

                {
                    title: 'test productg',
                    price: 13.5,
                    description: 'lorem ipsum set',
                    image: 'https://i.pravatar.cc',
                    category: 'electronic',

                },
                { headers: { 'content-type': 'application/json; charset=utf-8' } }
            )

            this.props.addProducts(res.data);

        } catch (error) {
            console.log(error);

        }

    }

    updateProduct = async (product) => {
        try {
            let res = await axios.put(`https://fakestoreapi.com/products/${product.id}`,
                {
                    title: product.title,
                    price: product.price,
                    description: product.description,
                    image: product.image,
                    category: product.category
                })

            let productData = res.data;
            console.log(productData);

            this.setState((state) => {
                return ({
                    ...state,
                    productData
                })
            })
        } catch (err) {
            console.error(err);
        }

    }





    deleteProduct = async (id) => {
        try {

            let res = await axios.delete(`https://fakestoreapi.com/products/${id}`);
            let data = res.data;
            this.props.deleteProductById(data.id)

        } catch (err) {
            console.error(err);
        }
    }

    render() {
        const { products, dataFetched, networkError } = this.state;
        console.log(this.props.allProducts)

        return (

            <div className="container ">
                {
                    networkError.length !== 0 ?
                        <div className="alert alert-danger text-center" role="alert">
                            {networkError}
                        </div>
                        :
                        undefined
                }
                {
                    networkError.length === 0 && this.props.allProducts.length === 0 && dataFetched ?
                        <div className="alert alert-warning text-center" role="alert">
                            No Products found
                        </div>
                        :
                        undefined
                }

                <div className="row justify-content-center">
                    {
                        dataFetched ?
                            this.props.allProducts.map((product) => {

                                return <div className="card d-flex justify-content-center align-items-center ms-2 g-2" style={{ width: "200px", height: "250px" }}>
                                    <img src={product.image} alt="product" className='w-25 img-fluid' />
                                    <h1 className='card-title text-center'>{product.title}</h1>
                                    <p><span>$</span>{product.price}</p>

                                    <div className="text-center">
                                        <ReactStars
                                            key={product.id}
                                            count={5}
                                            size={15}
                                            value={product.rating !== undefined ? product.rating.rate : undefined}
                                            isHalf={true}
                                            activeColor="#bc7940"
                                        />
                                    </div>
                                    <div className="d-flex w-50 justify-content-between">
                                        <UpdateProducts updateproduct={this.updateProduct} product={product} />
                                        <DeleteProduct id={product.id} deleteproduct={this.deleteProduct} />



                                    </div>

                                </div>
                            })
                            :

                            networkError.length !== 0 ?
                                undefined
                                :

                                <div className="spinner-border text-success" role="status">
                                    <span className="sr-only"></span>
                                </div>

                    }
                    {
                        dataFetched ?
                            <div>
                                <AddProduct addproduct={this.addProduct} products={products} />

                            </div>

                            :
                            undefined
                    }
                </div>

            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);

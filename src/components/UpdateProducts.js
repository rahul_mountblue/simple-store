import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';


class UpdateProducts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title: '',
            price: '',
            show: false,
        }

    }
    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value })
    }

    handleClick = () => {
        const {title,price} = this.state;
        if(title !== '' ) {
            this.props.product.title = title;
        }
        if(price !== '') {
            this.props.product.price = Number(price);
        }
        

        this.props.updateproduct(this.props.product);
        this. handleClose();
       
        
    }

    handleClose = () => {
        this.setState({ show: false })
    }

    handleShow = () => {
        this.setState({ show: true });
    }

    render() {
        const { idError, show } = this.state;

        return (

            <div>
                 <i className="fa fa-edit" style={{fontSize:"20px",color:"blue"}} onClick={this.handleShow}></i>
                

                <Modal show={show} onHide={this.handleClose}  centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit product details</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                        <form>
                            <div className="form-group">
                                <label htmlFor="forTitle">Title</label>
                                <input type="text"
                                    name='title'
                                    className="form-control"
                                    id="forTitle"
                                    defaultValue={this.props.product.title}
                                    onChange={this.handleChange}
                                    aria-describedby="emailHelp"

                                />

                            </div>
                            <div className="form-group">
                                <label htmlFor="forPrice">Price</label>
                                <input type="number"
                                    name='price'
                                    className="form-control"
                                    defaultValue={this.props.product.price}
                                    onChange={this.handleChange}
                                    id="forPrice" />
                            </div>


                        </form>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={this.handleClick}>
                            Save Changes
                        </Button>
                    </Modal.Footer>
                </Modal>


            </div>


        )
    }
}

export default UpdateProducts;

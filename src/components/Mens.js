import React, { Component } from 'react'
import axios from 'axios';
import ReactStars from "react-rating-stars-component";

class Mens extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mensproducts: [],
            dataFetched: false,
        }
    }

    componentDidMount() {
        this.getProductData();
    }

    async getProductData() {
        try {
            let products = await axios.get('https://fakestoreapi.com/products');
            let mensProduct = products.data.filter(data=> data.category === "men's clothing");
            this.setState({
                mensProducts: mensProduct,
                dataFetched: true,
            })
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        const { mensProducts, dataFetched } = this.state;
        return (
            <div className="container">
                <div className="row justify-content-center">
                {
                    dataFetched ?
                        mensProducts.map((product) => {

                            return <div className="card d-flex align-items-center ms-2 g-2" style={{width:"200px"}}>
                            <img src={product.image} alt="product" className='w-25 h-50 img-fluid'/>
                            <h1 className='card-title text-center'>{product.title}</h1>
                            <p><span>$</span>{product.price}</p>
                            <div className="text-center">
                                <ReactStars
                                    count={5}
                                    size={15}
                                    value={product.rating.rate}
                                    isHalf={true}
                                    activeColor="#bc7940"
                                />
                            </div>
                        </div>
                    })
                    :
                    <div class="spinner-border text-success" role="status">
                    <span class="sr-only"></span>
                  </div>
                }
                </div>
                

            </div>
        )
    }
}

export default Mens;

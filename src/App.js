import React, { Component } from 'react'
import './css/reset.css'
//import './css/style.css'
import Products from './components/Products';
import Navbar from './components/Navbar';
import Banner from './components/Banner'
import Categories from './components/Categories';
import Mens from './components/Mens'
import Womens from './components/Womens';
import Electronics  from './components/Electronics';
import AddProduct from './components/AddProduct';
import {Switch,Route} from 'react-router-dom';
import Login from './components/Login'

class App extends Component {
  
  render() {
    return (
      <div className="container-fluid">
          <Navbar />
          <Banner />
          <Categories />
         <Switch>
           <Route exact path="/" component={Products} />
           <Route exact path="/men" component={Mens} />
           <Route exact path="/women" component={Womens} />
           <Route exact path="/electronic" component={Electronics} />
           <Route exact path="/addproduct" component={AddProduct} />
           <Route exact path="/login" component={Login} />
          </Switch>
      </div>
    )
  }
}

export default  App;

